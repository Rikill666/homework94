const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PostSchema = new Schema({
  user:{
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  dateTime:{
    type: Date,
    default: new Date
  },
  text: {
    type: String,
  },
  image: String,
  tags: [String]
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;