const mongoose = require('mongoose');
const config = require('./config');
const Post = require('./models/Post');
const User = require('./models/User');
const nanoid = require("nanoid");

const run = async () => {
  await mongoose.connect(config.database, config.databaseOptions);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (let coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    username: 'user',
    password: '123',
    token: nanoid(),
  }, {
    username: 'admin',
    password: 'admin123',
    role: 'admin',
    token: nanoid(),
    subscriptions:[user._id]
  });


  await Post.create({
    user: user,
    text: "vlfdfdvfdvfdvf",
    image: 'uploads/fixtures/cpu.jpg'
  }, {
    user: user,
    text: "bdfbfgbgdfbfdbfdbffdvfd",
    image: 'uploads/fixtures/cpu.jpg'
  }, {
    user: admin,
    text: "vfdvdfvfdvdfdvdf",
    image: 'uploads/fixtures/cpu.jpg'
  });

  mongoose.connection.close();
};

run().catch(e => {
  mongoose.connection.close();
  throw e;
});