const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;

const auth = require('../middleware/auth');
const upload = require('../multer').uploads;

const Post = require('../models/Post');

const router = express.Router();

router.get('/', auth, async (req, res) => {
    const user = req.user;
    const items = await Post.find({user: {$in: [...user.subscriptions, user._id]}}).sort("-dateTime").populate('user', 'username displayName');
    res.send(items);
});

router.get('/tags', async (req, res) => {
    const tags = await Post.distinct('tags');

    return res.send(tags);
});


router.post('/', [auth, upload.single('image')], async (req, res) => {
    if (!req.file && !req.body.text) {
        return res.status(400).send({errors: {text: {message: "Fill in the text or image field"}}});
    }
    try {
        const user = req.user;
        const postData = {
            text: req.body.text,
            user: user,
            tags: JSON.parse(req.body.tags)
        };

        if (req.file) {
            postData.image = req.file.filename;
        }

        const post = new Post(postData);

        await post.save();

        return res.send({id: post._id});
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            return res.sendStatus(500);
        }
    }
});


module.exports = router;