import React, {useState} from 'react';
import {Link} from "react-router-dom";
import noAvatar from "../../../assets/images/no-avatar-300x300.png";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import {apiURL} from "../../../constants";
import Avatar from "@material-ui/core/Avatar";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(2),
        },
    },

}));

const UserMenu = ({user, logout}) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    let avatar = noAvatar;
    if(user.avatar){
        avatar = user.avatar.indexOf('https://') >= 0 ? user.avatar : apiURL + '/' + user.avatar;
    }
    return (
        <>
            <IconButton color="inherit" onClick={handleClick}>
                <div className={classes.root}>
                    <Avatar alt={user.username} src={avatar}/>
                </div>
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <ListItem disabled>Hello, {user.displayName || user.username}!</ListItem>
                <Divider/>
                <MenuItem onClick={handleClose} component={Link} to="/profile">Profile</MenuItem>
                <MenuItem onClick={logout}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;