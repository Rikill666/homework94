import React from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {Card} from "@material-ui/core";
import CardMedia from "@material-ui/core/CardMedia";
import {makeStyles} from "@material-ui/core/styles";
import imageNotAvailable from "../../assets/images/image_not_available.jpg";
import {apiURL} from "../../constants";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Moment from "react-moment";
import CardActions from "@material-ui/core/CardActions";

const useStyles = makeStyles({
    card: {
        width: "50%",
        display: "inline-block",
        textAlign: 'left',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    cardShell: {
        textAlign: 'center',
    }
});

const PostListItem = props => {
    const classes = useStyles();

    let image = imageNotAvailable;

    if (props.image) {
        image = apiURL + '/' + props.image.replace('\\', '/');
    }

    return (
        <Grid item xs={9} className={classes.cardShell}>
            <Card className={classes.card}>
                <CardMedia image={image} title={props.text} className={classes.media}/>
                <CardContent>
                    <Typography variant="body2" component="p">
                        {props.text}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Grid container direction="row" spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <Typography gutterBottom variant="h6" component="h6">
                                {props.username}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Typography variant="body2" color="textSecondary" component="p">
                                <Moment format="YYYY-MM-DD HH:mm:hh">
                                    {props.date}
                                </Moment>
                            </Typography>
                        </Grid>
                    </Grid>

                </CardActions>
            </Card>
        </Grid>
    );
};

PostListItem.propTypes = {
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
};

export default PostListItem;