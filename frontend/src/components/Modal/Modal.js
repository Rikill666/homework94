import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import FormElement from "../UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import {closeModal, subscribeUser} from "../../store/actions/usersActions";


const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);


function Modal (){
    const showModal = useSelector(state => state.users.showModal)||false;
    const subscribeError = useSelector(state => state.users.subscribeError);
    const dispatch = useDispatch();

    const [username, setUsername] = React.useState("");

    const inputChangeHandler = (event) => {
        setUsername(event.target.value)
    };

    const handleClose = async() => {
        await dispatch(closeModal(showModal));
    };

    const subscribe = async() => {
        await dispatch(subscribeUser(username));
        if(!subscribeError){
            setUsername("");
            await dispatch(closeModal(showModal));
        }
    };

    return (
        <div>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={showModal}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Подписка
                </DialogTitle>
                <DialogContent dividers>
                    <FormElement
                        error={subscribeError}
                        propertyName="username"
                        title="Enter username"
                        value={username}
                        onChange={inputChangeHandler}
                        type="text"
                    />
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={subscribe} color="primary">
                        Подписаться
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
export default Modal;