import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Form} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {editProfile} from "../../store/actions/usersActions";
import {apiURL} from "../../constants";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Toolbar from "@material-ui/core/Toolbar";
import noAvatar from "../../assets/images/no-avatar-300x300.png";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(2),
        },
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
}));

const UserProfile = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const [state, setState] = useState({
        password: '',
        username: user.username || '',
        avatar: user.avatar || null,
        displayName: user.displayName || '',
    });

    const onSubmit = e => {
        e.preventDefault();

        const profileData = new FormData();

        Object.keys(state).forEach(key => {
            profileData.append(key, state[key]);
        });

        dispatch(editProfile(profileData));
    };

    const onChange = e => {
        setState({...state, [e.target.name]: e.target.value});
    };

    const onFileChange = e => {
        setState({...state, [e.target.name]: e.target.files[0]});
    };
    let avatar = noAvatar;
    if(user.avatar){
        avatar = user.avatar.indexOf('https://') >= 0 ? user.avatar : apiURL + '/' + user.avatar;
    }
    return (
        <>
            <Toolbar/>
            <h3>Change user profile</h3>
            <Form onSubmit={onSubmit}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <FormElement
                            type="password"
                            propertyName="password"
                            title="Change password"
                            onChange={onChange}
                            value={state.password}
                        />
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="file"
                            propertyName="avatar"
                            title="Upload avatar"
                            onChange={onFileChange}
                        />
                    </Grid>
                    <Grid container direction="row" spacing={2}>
                        <Grid item xs={3}>
                            Currently selected avatar:
                        </Grid>
                        <Grid item xs={9}>
                                <div className={classes.root}>
                                    <Avatar alt={user.username} src={avatar} className={classes.large}/>
                                </div>
                        </Grid>
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            propertyName="displayName"
                            title="DisplayName"
                            onChange={onChange}
                            value={state.displayName}
                        />
                    </Grid>
                    {!user.username_changed ?
                        <Grid item xs>
                            <FormElement
                                propertyName="username"
                                title="User name"
                                onChange={onChange}
                                value={state.username}
                            />
                        </Grid> : null}
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">
                            Edit profile
                        </Button>
                    </Grid>
                </Grid>
            </Form>
        </>
    );
};

export default UserProfile;