import React, {Component} from 'react';
import {fetchPosts} from "../../store/actions/postsActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import PostListItem from "../../components/PostListItem/PostListItem";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Modal from "../../components/Modal/Modal";
import {closeModal} from "../../store/actions/usersActions";

class Posts extends Component {

    componentDidMount() {
        this.props.fetchPosts();
    }


    render() {
        return (
            <Grid container direction="column" spacing={2}>
                <Toolbar/>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">
                            {this.props.posts.length > 0 ? "Posts" : "No Posts"}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Button variant="outlined" color="primary" onClick={()=>this.props.closeModal(this.props.showModal)}>
                            Subscribe
                        </Button>

                    </Grid>
                    <Grid item>
                        <Button
                            color="primary"
                            component={Link}
                            to={"/post/new"}
                        >
                            Add post
                        </Button>
                    </Grid>
                </Grid>
                <Modal/>
                <Grid item container direction="row" spacing={1} justify="center">
                    {this.props.posts.map(post => {
                        return <PostListItem
                            key={post._id}
                            text={post.text}
                            id={post._id}
                            image={post.image}
                            date={post.dateTime}
                            username={post.user.displayName || post.user.username}
                        />
                    })}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts,
    showModal: state.users.showModal
});

const mapDispatchToProps = dispatch => ({
    fetchPosts: () => dispatch(fetchPosts()),
    closeModal: (showModal) => dispatch(closeModal(showModal)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
