import React, {Component, Fragment} from 'react';
import PostForm from "../../components/PostForm/PostForm";
import {createPost, fetchTags} from "../../store/actions/postsActions";
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";

class NewPost extends Component {
    componentDidMount() {
        this.props.fetchTags();
    }

    createPost = async (postData) => {
        await this.props.createPost(postData);
    };

    render() {
        return (
            <Fragment>
                <Toolbar/>
                <Box pb={2} pt={2}>
                    <Typography variant="h4">New post</Typography>
                </Box>

                <PostForm
                    error = {this.props.createError}
                    onSubmit={this.createPost}
                    tags={this.props.tags}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tags: state.posts.tags,
    createError: state.posts.createError
});

const mapDispatchToProps = dispatch => ({
    createPost: postData => dispatch(createPost(postData)),
    fetchTags: () => dispatch(fetchTags()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPost);