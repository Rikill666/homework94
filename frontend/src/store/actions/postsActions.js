import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';

export const CREATE_POST_REQUEST = 'CREATE_POST_REQUEST';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_POST_ERROR = 'CREATE_POST_ERROR';

export const FETCH_TAGS_REQUEST = 'FETCH_TAGS_REQUEST';
export const FETCH_TAGS_SUCCESS = 'FETCH_TAGS_SUCCESS';
export const FETCH_TAGS_ERROR = 'FETCH_TAGS_ERROR';

export const fetchPostsRequest = () => {return {type: FETCH_POSTS_REQUEST};};
export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchPostsError = (error) => {return {type: FETCH_POSTS_ERROR, error};};

export const createPostRequest = () => {return {type: CREATE_POST_REQUEST};};
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostError = (error) => {return {type: CREATE_POST_ERROR, error};};

export const fetchTagsRequest = () => {return {type: FETCH_TAGS_REQUEST};};
export const fetchTagsSuccess = tags => ({type: FETCH_TAGS_SUCCESS, tags});
export const fetchTagsError = (error) => ({type: FETCH_TAGS_ERROR, error});


export const fetchPosts = () => {
  return async dispatch => {
    try{
      dispatch(fetchPostsRequest());
      const response = await axiosApi.get('/posts');
      const posts = response.data;
      dispatch(fetchPostsSuccess(posts));
    }
    catch (e) {
      dispatch(fetchPostsError(e));
    }
  };
};
export const fetchTags = () => {
  return async dispatch => {
    try{
      dispatch(fetchTagsRequest());
      const response = await axiosApi.get('/posts/tags');
      dispatch(fetchTagsSuccess(response.data));
    }
    catch (e) {
      dispatch(fetchTagsError(e));
    }
  };
};

export const createPost = (postData) => {
  return async dispatch => {
    try{
      dispatch(createPostRequest());
      await axiosApi.post('/posts', postData);
      dispatch(createPostSuccess());
      dispatch(push('/'));
    }
    catch (error) {
      if(error.response){
        dispatch(createPostError(error.response.data));
      }
      else{
        dispatch(createPostError({global:"Network error or no internet"}));
      }
    }
  };
};


