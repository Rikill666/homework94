import {
  CREATE_POST_ERROR,
  CREATE_POST_REQUEST,
  CREATE_POST_SUCCESS, FETCH_POSTS_ERROR,
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_TAGS_SUCCESS
} from "../actions/postsActions";

const initialState = {
  posts: [],
  tags: [],
  error: null,
  createError:null,
  loading: false
};

const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_REQUEST:
      return {...state, loading: true};
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts, error:null, loading: false};
    case FETCH_POSTS_ERROR:
      return {...state, error: action.error, loading: false};

    case CREATE_POST_REQUEST:
      return {...state, loading: true};
    case CREATE_POST_SUCCESS:
      return {...state, createError:null, loading: false};
    case CREATE_POST_ERROR:
      return {...state, createError: action.error, loading: false};

    case FETCH_TAGS_SUCCESS:
      return {...state, tags: action.tags};

    default:
      return state;
  }
};

export default postsReducer;